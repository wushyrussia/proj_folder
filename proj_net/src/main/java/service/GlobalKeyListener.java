package service;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.sun.java.accessibility.util.AWTEventMonitor.addKeyListener;

public class GlobalKeyListener implements NativeKeyListener {

    /**
     * Ctrl + Shift + S == start
     * Ctrl + Shift + Q == stop
     */
    private boolean ctrl = false, shift = false, s = false, q = false;
    public static int start = 0;
    public static int stop = 1;
    public void nativeKeyPressed(NativeKeyEvent e) {
        if (e.getKeyCode() == NativeKeyEvent.VC_CONTROL_L) {
            ctrl = true;
//            System.out.println("Ctrl");
        }
        if (e.getKeyCode() == NativeKeyEvent.VC_SHIFT_L) {
            shift = true;
//            System.out.println("Shift");
        }
        if (e.getKeyCode() == NativeKeyEvent.VC_S) {
            s = true;
            if (s && ctrl && shift) {
                System.out.println("Wtf bugg");
                start = 1;
            }
            shift = false;
            ctrl = false;
            s = false;
        } else {
            if (e.getKeyCode() == NativeKeyEvent.VC_Q) {
                q = true;
                if (q && ctrl && shift) {
                    System.out.println("Wtf STOOOOOP");
                }
                shift = false;
                ctrl = false;
                s = false;
                q = false;
                stop = 0;
            }
        }
    }

    public void nativeKeyReleased(NativeKeyEvent e) {
        if (e.getKeyCode() == NativeKeyEvent.VC_A) {
            ctrl = false;
        } else if (e.getKeyCode() == NativeKeyEvent.VC_W) {
            shift = false;
        } else if (e.getKeyCode() == NativeKeyEvent.VC_W) {
            s = false;
        }
    }

    public void nativeKeyTyped(NativeKeyEvent e) {
        //System.out.println("Key Typed: " + e.getKeyText(e.getKeyCode()));

    }
}
