
import lombok.Getter;

import java.awt.*;
import java.awt.image.BufferedImage;

public class main {
    public static void main(String argc[]){

    }

    @Getter
    public static class ShotArea {
        private Rectangle area;
        public ShotArea(int x, int y, int width, int height){
            area = new Rectangle(x,y,width,height);
        }
        public void getScreenValue(Rectangle area) throws AWTException {
            Rectangle screenRect = new Rectangle(area);
            BufferedImage capture = new Robot().createScreenCapture(screenRect);

        }
    }
}
