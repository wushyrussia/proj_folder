package com.home.work.service.impl;

import com.home.work.service.TimerService;
import org.springframework.stereotype.Component;

@Component
public class TimerServiceImpl implements TimerService {
    public TimerServiceImpl() {
        System.out.println("TimeService Launched. called and  initialized");
    }
    public void timerOn(int time){
        System.out.println("The program will be launched in "+time+" minutes (TimerService)");
    }
    public void noTimer(){
        System.out.println("The program will be launched now. (TimerService)");
    }
}
