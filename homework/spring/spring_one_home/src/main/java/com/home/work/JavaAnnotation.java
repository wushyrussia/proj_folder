package com.home.work;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScans(value = {
        @ComponentScan(basePackages = {"com.home.work.service.impl"}, lazyInit = true)
//        @ComponentScan(basePackages = {"com.mycompany.service1"}, lazyInit = false)
})
public class JavaAnnotation {
}
