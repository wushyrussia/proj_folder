package com.home.work.service.impl;

import com.home.work.service.ChooseService;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
@Component
public class ChooseServiceImpl implements ChooseService {
    public ChooseServiceImpl() {
        System.out.println("ChooseService Launched.  called and  initialized");
    }

    private Map<Long, String> application = new HashMap<>();
    {
        application.put(1L, "Skype");
        application.put(2L, "Chrome");
        application.put(3L, "Winamp");
        application.put(4L, "Ultra ISO");
    }
    public String chooseApp(long id){
        return application.get(id);
    }
}
