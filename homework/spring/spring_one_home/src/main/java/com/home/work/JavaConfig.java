package com.home.work;

import com.home.work.service.ConstrArgService;
import com.home.work.service.MessageService;
import com.home.work.service.impl.ChooseServiceImpl;
import com.home.work.service.impl.LaunchServiceImpl;
import com.home.work.service.impl.MessageServiceImpl;
import com.home.work.service.impl.TimerServiceImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JavaConfig {
    @Bean
    @Scope()
    public LaunchServiceImpl launchServiceBean() {
        LaunchServiceImpl launchServiceBean = new LaunchServiceImpl();
        launchServiceBean.setChooseService(chooseService());
        launchServiceBean.setTimerService(timerService());
        return launchServiceBean;
    }
    @Bean
    public ChooseServiceImpl chooseService(){
        return new ChooseServiceImpl();
    }
    @Bean
    public TimerServiceImpl timerService(){
        return new TimerServiceImpl();
    }
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public MessageServiceImpl toastBeanSingleton(){
        return new MessageServiceImpl();
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public MessageServiceImpl toastBeanPrototype(){
        return new MessageServiceImpl();
    }
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public ConstrArgService constrArgService(){
        return new ConstrArgService();
    }
}
