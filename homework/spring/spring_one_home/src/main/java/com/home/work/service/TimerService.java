package com.home.work.service;

public interface TimerService {
    public void timerOn(int time);
    public void noTimer();
}
