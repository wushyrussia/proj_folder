package com.home.work;

import com.home.work.service.LaunchService;
import com.home.work.service.MessageService;
import com.home.work.service.impl.LaunchServiceImpl;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;


public class MainTestBeanXml
{
    @Test
    public void TestBeanXml(){
        System.out.println("_______________First____________________");
        final ApplicationContext APPLICATION_CONTEXT =
                new ClassPathXmlApplicationContext("beans.xml");


            final LaunchService myService = (LaunchService) APPLICATION_CONTEXT.getBean("launchServiceBean");
            myService.launcher(4,1);

            final MessageService toastService = (MessageService) APPLICATION_CONTEXT.getBean("ToastBeanSingleton");
            toastService.getMessage("Singleton");
            final MessageService toastService2 = (MessageService) APPLICATION_CONTEXT.getBean("ToastBeanSingleton");
            toastService2.getMessage("Singleton 2");
            System.out.println("\n");
            final MessageService toastServiceProt = (MessageService) APPLICATION_CONTEXT.getBean("ToastBeanPrototype");
            toastServiceProt.getMessage("Prototype");
            final MessageService toastServiceProt2 = (MessageService) APPLICATION_CONTEXT.getBean("ToastBeanPrototype");
            toastServiceProt2.getMessage("Prototype 2");
        System.out.println("_______________First____________________");
    }

    @Test
    public void TestJavaConfiguration(){
        System.out.println("*********************Second***************************");
        final ApplicationContext APPLICATION_CONTEXT =
                new AnnotationConfigApplicationContext(JavaConfig.class);

        final LaunchService myService = (LaunchService) APPLICATION_CONTEXT.getBean("launchServiceBean");
        myService.launcher(4,1);

        final MessageService toastService = (MessageService) APPLICATION_CONTEXT.getBean("toastBeanSingleton");
        toastService.getMessage("Singleton");
        final MessageService toastService2 = (MessageService) APPLICATION_CONTEXT.getBean("toastBeanSingleton");
        toastService2.getMessage("Singleton 2");
        System.out.println("\n");
        final MessageService toastServiceProt = (MessageService) APPLICATION_CONTEXT.getBean("toastBeanPrototype");
        toastServiceProt.getMessage("Prototype");
        final MessageService toastServiceProt2 = (MessageService) APPLICATION_CONTEXT.getBean("toastBeanPrototype");
        toastServiceProt2.getMessage("Prototype 2");
        System.out.println("**************************Second**********************");

    }

    @Test
    public void TestJavaAnnotation(){
        System.out.println("*************************Third***********************");
        final ApplicationContext APPLICATION_CONTEXT =
                new AnnotationConfigApplicationContext(JavaAnnotation.class);

        final LaunchService myService = (LaunchService) APPLICATION_CONTEXT.getBean(LaunchService.class);
        myService.launcher(4,1);
        System.out.println("**************************Third**********************");

    }
}