drop table categories;
drop table items;
drop table brand;
CREATE DATABASE modoshop;
CREATE USER modoshop WITH password 'modoshop';
GRANT ALL ON DATABASE modoshop TO modoshop;

CREATE TABLE categories (
  id SERIAL PRIMARY KEY UNIQUE,
  parentCat int,
  catName CHAR(64));

CREATE table brand (
id SERIAL primary key unique ,
name varchar not null ,
img varchar
);

CREATE TABLE items (
  id SERIAL PRIMARY KEY,
  id_cat int ,
  brand_id int,
  rating int,
  price DECIMAL,
  img varchar,
  description varchar,
  article decimal,
  material varchar,
  size varchar,
  color varchar,
  Constraint id_cat_fk foreign key  (id_cat)
                  references categories(id),
  constraint brand_id_fk foreign key (brand_id)
                  references  brand(id)
);
