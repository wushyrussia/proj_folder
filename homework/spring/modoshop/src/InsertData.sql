--categories
INSERT INTO categories VALUES (DEFAULT ,0,'Женщинам');
INSERT INTO categories VALUES (default ,0,'Мужчинам');
INSERT INTO categories VALUES (default ,0,'Обувь');
INSERT INTO categories VALUES (default ,0,'Бренды');
INSERT INTO categories VALUES (default ,0,'Распродажа');

--brand
INSERT INTO brand VALUES (default ,'Odijj');
INSERT INTO brand VALUES (default ,'Nike');
INSERT INTO brand VALUES (default ,'Spark');
INSERT INTO brand VALUES (default ,'Linklein');
INSERT INTO brand VALUES (default ,'Mazzory');
--
INSERT INTO items VALUES (DEFAULT,1,2,3,3000,'/img/mod1.png','any desription 1 item',1,'Polyester','1,2,3','red');
INSERT INTO items VALUES (DEFAULT,1,3,1,4900,'/img/mod2.png','any desription 2 item',2,'Polyester','1,2,3','red and blue');
INSERT INTO items VALUES (DEFAULT,2,1,4,200,'/img/mod3.png','any desription 3 item',3,'wool','1,2,3','white');
INSERT INTO items VALUES (DEFAULT,2,3,2,5900,'/img/mod4.png','any desription 4 item',4,'wool','1,2,3','black');
INSERT INTO items VALUES (DEFAULT,3,4,5,3230,'/img/mod5.png','any desription 5 item',5,'EVA','1,2,3','purple');
INSERT INTO items VALUES (DEFAULT,3,1,5,12000,'/img/mod6.png','any desription 6 item',6,'EVA','3','aqua');