package modoshop.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "items")
public class Item {
    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "rating", length = 1)
    private int rating;

    @Column(name = "price", nullable = false)
    private long price;

    @Column(name = "img")
    private String img;

    @Column(name = "description",nullable = false)
    private String description;

    @Column(name = "article",nullable = false)
    private Long article;

    @Column(name = "material",nullable = false)
    private String material;

    @Column(name = "size",nullable = false)
    private String size;

    @Column(name = "color",nullable = false)
    private String color;


    @ManyToOne
    @JoinColumn(name ="id_cat")
    private Categories categories;
    @ManyToOne
    @JoinColumn(name ="brand_id")
    private Brand brand;
}
