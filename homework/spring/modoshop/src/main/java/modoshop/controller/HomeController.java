package modoshop.controller;

        import modoshop.model.Item;
        import modoshop.model.Category;
        import modoshop.repository.CatRepos;
        import modoshop.repository.ItemRepository;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Controller;
        import org.springframework.ui.Model;
        import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @Autowired
    ItemRepository itemRepository;
    @Autowired
    CatRepos catRepos;
    @GetMapping("/")
    public String home(Model model) {

        model.addAttribute("items", itemRepository.findAll());
//        model.addAttribute("NavItems", Categories.items);
        return "index";
    }
}

