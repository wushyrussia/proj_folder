//package modoshop.controller;
//
//import modoshop.model.Item;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//@Controller
//public class ItemController {
//
//    @GetMapping("/item/{id}")
//    public String item_card(@PathVariable Integer id, Model model) {
//
//        model.addAttribute("item", Item.items.get(id));
//        return "item_card";
//    }
//}
