package modoshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModoshopApplication {

	public static void main(String[] args) {

		SpringApplication.run(ModoshopApplication.class, args);
	}

}
