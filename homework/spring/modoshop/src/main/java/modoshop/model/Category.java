package modoshop.model;

        import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class Category {
    String mainCategory;

    Category(String itemMenu){
        this.mainCategory = itemMenu;
    }

    public   static List<Category> items = new ArrayList<Category>();
    static {
        items.add(new Category("Женщинам"));
        items.add(new Category("Мужчинам"));
        items.add(new Category("Обувь"));
        items.add(new Category("Аксессуры"));
        items.add(new Category("Бренды"));
        items.add(new Category("Распродажа"));
    }
    /*As toString()*/
   public String catAsText(){
        return mainCategory;
   }
}