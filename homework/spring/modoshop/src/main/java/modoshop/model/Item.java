package modoshop.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Item {
    private String brand;
    private int rating;
    private long price;
    private String img;
    private String description;
    private Long id;
    private Long article;
    private String material;
    private String size;
    private String color;

    public Item(String brand, int rating, long price, String img, String descripion, Long id, Long article, String material){
        this.brand = brand;
        this.rating = rating;
        this.price=price;
        this.img=img;
        this.description = descripion;
        this.id=id;
        this.article = article;
        this.material = material;
        }

//  public   static List<Item> items = new ArrayList<Item>();
//    static {  items.add(new Item("Jeans", "GJ",0,1999,"img/mod1.png","item 1. Прекрасный спортивный костюм, для активных и стильных девушек. Костюм состоит из удобных брюк на эластичном поясе с завязками и тостовски с капюшоном и застежкой на молнию. Толстовка украшена рисунком в горох.",0,122,"Хлопок(100%)"));
//        items.add(new Item("Jeans", "JeLe",10,600,"img/mod2.png", "item 2. Прекрасный спортивный костюм, для активных и стильных девушек. Костюм состоит из удобных брюк на эластичном поясе с завязками и тостовски с капюшоном и застежкой на молнию. Толстовка украшена рисунком в горох.",1,23123,"Хлопок(80%)"));
//        items.add(new Item("blouse", "GJ",40,3099,"img/mod3.png","item 3. Прекрасный спортивный костюм, для активных и стильных девушек. Костюм состоит из удобных брюк на эластичном поясе с завязками и тостовски с капюшоном и застежкой на молнию. Толстовка украшена рисунком в горох.",2,454,"Хлопок(60%)"));
//        items.add(new Item("blouse", "GJ",30,1200,"img/mod4.png","item 4. Прекрасный спортивный костюм, для активных и стильных девушек. Костюм состоит из удобных брюк на эластичном поясе с завязками и тостовски с капюшоном и застежкой на молнию. Толстовка украшена рисунком в горох.",3,2222,"Хлопок(90%)"));
//        items.add(new Item("hat", "JeLe",0,999,"img/hat.png", "item 5. Прекрасный спортивный костюм, для активных и стильных девушек. Костюм состоит из удобных брюк на эластичном поясе с завязками и тостовски с капюшоном и застежкой на молнию. Толстовка украшена рисунком в горох.",4,2234,"Хлопок(70%)"));
//    }
}
