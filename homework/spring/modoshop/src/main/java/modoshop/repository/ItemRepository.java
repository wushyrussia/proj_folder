package modoshop.repository;

import modoshop.entity.Item;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ItemRepository extends CrudRepository <Item,Long>{
//    @Query("from Item INNER JOIN categories on Item.id_cat = Categories.id")
//    List <Item> getAllMy();
//    @Query("from Item")
//    List <Item>getAllMy();
}
