drop database example;
drop user user2;

CREATE DATABASE example;
CREATE USER user2 WITH PASSWORD 'example';
GRANT ALL PRIVILEGES ON DATABASE example to user2;

CREATE TABLE usern(
  id BIGINT PRIMARY KEY ,
  username VARCHAR (50) UNIQUE not null ,
  password VARCHAR (50) not null check (length(password) >= 6 ),
  email VARCHAR (355) unique
);
CREATE TABLE  tag(
  id BIGINT primary key ,
  tag varchar(50)
);
CREATE TABLE entry(
  id BIGINT primary key,
  title VARCHAR(50) not null ,
  entryText varchar(9000) check (length(entryText) >= 3),
  date varchar(20) not null,
  tag_id bigint references tag(id),
  usern_id bigint not null references usern(id)
);
CREATE SEQUENCE USER_SEQUENCE START WITH 1 INCREMENT BY 3;
CREATE SEQUENCE ENTRY_SEQUENCE START WITH 1 INCREMENT BY 3;
CREATE SEQUENCE TAG_SEQUENCE START WITH 1 INCREMENT BY 3;

-- drop table usern cascade ;
-- drop table tag cascade ;
-- drop table entry cascade ;