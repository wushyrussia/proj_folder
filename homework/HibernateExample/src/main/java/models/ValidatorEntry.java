package models;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {Validator.class})
@Documented
public @interface ValidatorEntry {

    String message() default "Failed validator test";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

