package models;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class Validator implements ConstraintValidator<ValidatorEntry, Entry> {

    public void initialize(ValidatorEntry validatorEntry) {
    }

    public boolean isValid(Entry entry, ConstraintValidatorContext constraintValidatorContext) {

        return (entry.getTitle().length() >= 3 && entry.getEntry().length() >= 3);

    }

}
