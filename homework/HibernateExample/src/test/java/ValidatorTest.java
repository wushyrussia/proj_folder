import models.Entry;
import models.Tag;
import models.UserN;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.testng.annotations.Test;
import util.SessionUtil;

public class ValidatorTest {

    private Object persist(Object entity) {
        try (Session session = SessionUtil.getSession()) {
            Transaction tx = session.beginTransaction();
            session.persist(entity);
            tx.commit();
        }
        return entity;
    }

    @Test
    public void testValidTrue() {
        Tag tag = new Tag();
        tag.setTag("firstTag");
        persist(tag);
        UserN userN = UserN.builder().email("one@mail.uu").username("myFirstName").password("1231456").build();
        persist(userN);
        Entry entry = Entry.builder().entry("Long first long").title("first").userN(userN).date("11.3.19").tag(tag).build();
        persist(entry);
    }
    @Test
    public void testValidUsername() {
        UserN userN = UserN.builder().email("user@mail.uu").username("myUserName1").password("sdsa1321331").build();
        persist(userN);
        Entry entry = Entry.builder().entry("Long second long").title("o").userN(userN).date("11.4.19").build();
        persist(entry);
    }
    @Test
    public void testValidPass() {
        UserN userN = UserN.builder().email("on@mail.uu").username("myThirdName").password("12345").build();
        persist(userN);
        Entry entry = Entry.builder().entry("Long third long").title("o").userN(userN).date("empty Date").build();
        persist(entry);
    }
    @Test
    public void testValidNotNull() {
        UserN userN = UserN.builder().email("on@mail.uu").username("myFourName").password("12345").build();
        persist(userN);
        Entry entry = Entry.builder().entry("Long four long").title("o").userN(userN).date("").build();
        persist(entry);
    }
}